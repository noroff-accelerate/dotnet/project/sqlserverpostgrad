﻿using System;
using Microsoft.Data.SqlClient;

namespace PostGradManagerSQLServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(130, 40);
            PrintHeader();
            // Build connection string (CHANGE TO YOUR INSTANCE)
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "DESKTOP-4J3HI0S\\SQLEXPRESS01";
            builder.InitialCatalog = "PostGrad";
            builder.IntegratedSecurity = true;

            // Connect to the SQLServer instance
            Console.Write("Connecting to SQL Server ... ");

            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    Console.WriteLine("Done. \n");

                    //Set Read operation (CHANGE TO YOUR TABLE/s)
                    string sql = "SELECT * FROM professor";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Console.WriteLine($"{reader.GetName(0)} \t {reader.GetName(1)}");
                            Console.WriteLine("--------------------------------");

                            while (reader.Read())
                            {
                                Console.WriteLine($"{reader.GetInt32(0)} \t {reader.GetString(1)}");
                            }

                            Console.WriteLine("----------------------------------");

                            Console.WriteLine(reader.FieldCount);
                            Console.WriteLine(reader.IsClosed);
                            Console.WriteLine(reader.VisibleFieldCount);
                            Console.WriteLine(reader.GetName(2));
                            Console.WriteLine(reader.GetDataTypeName(0));

                            #region Example CRUD Operations

                            //string sql = "INSERT INTO Professor (First_Name, Last_Name, Subject) VALUES (@FirstName, @LastName, @Subject)";
                            //string sql = "DELETE FROM Professor WHERE ID = @professorID";
                            //string sql = "UPDATE Professor SET Last_Name = @newLastName WHERE ID = @professorID";

                            //string firstName = "Prof Kit";
                            //string lastName = "Dale";
                            //string subject = "Sub Saharan Bush Fires";

                            //int professorID = 2;
                            //string lastName = "Nutini";                    
                            //command.Parameters.AddWithValue("@newLastName", lastName);
                            //command.Parameters.AddWithValue("@professorID", professorID);

                            //command.ExecuteNonQuery();

                            //command.Parameters.AddWithValue("@FirstName", firstName);
                            //command.Parameters.AddWithValue("@FirstName", firstName);
                            //command.Parameters.AddWithValue("@LastName", lastName);
                            //command.Parameters.AddWithValue("@Subject", subject);
                            //string sql = "INSERT INTO Professor (First_Name, Last_Name, Subject) VALUES (@FirstName, @LastName, @Subject)";

                            //string firstName = "Prof Kit";
                            //string lastName = "Dale";
                            //string subject = "Sub Saharan Bush Fires";

                            #endregion
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void PrintHeader()
        {
            string header = @"

 _____  _____ _      _____                           ______         _   _____               ____  ___                 
/  ___||  _  | |    /  ___|                          | ___ \       | | |  __ \             | |  \/  |                 
\ `--. | | | | |    \ `--.  ___ _ ____   _____ _ __  | |_/ /__  ___| |_| |  \/_ __ __ _  __| | .  . |_ __   __ _ _ __ 
 `--. \| | | | |     `--. \/ _ \ '__\ \ / / _ \ '__| |  __/ _ \/ __| __| | __| '__/ _` |/ _` | |\/| | '_ \ / _` | '__|
/\__/ /\ \/' / |____/\__/ /  __/ |   \ V /  __/ |    | | | (_) \__ \ |_| |_\ \ | | (_| | (_| | |  | | | | | (_| | |   
\____/  \_/\_\_____/\____/ \___|_|    \_/ \___|_|    \_|  \___/|___/\__|\____/_|  \__,_|\__,_\_|  |_/_| |_|\__, |_|   
                                                                                                            __/ |     
                                                                                                           |___/      
            _.-'`'-._
         .-'    _    '-.
          `-.__  `\_.-'
            |  `-``\|
            `-.....-A
";

            Console.WriteLine(header);
        }
    }
}